package properties;



public class ProjectProperties {
	

	public static class Settings {

		public static final String READER_FILE_PATH = "/res/xml_books_list.xml";
		public static final String EDITOR_FILE_PATH = "res/xml_books_list.xml";
		
				
				
		public static final String LOCAL = "LOCAL";
		public static final String XML = "XML";
		
		public static String DATA_SOURCE = LOCAL;

		public void setLocalDataSource() {
			
			
			Settings.DATA_SOURCE = LOCAL;

		}

		public void setXMLDataSource() {
			Settings.DATA_SOURCE = XML;

		}

	}

}
