package library.handlers;

import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.UISynchronize;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.swt.widgets.Shell;

import properties.ProjectProperties;
import properties.ProjectProperties.Settings;
import services.XMLBackgroundService;

public class ChangeToXMLDataSource {

	
	@Inject
	IEventBroker eventBroker;
	
	@Inject UISynchronize sync;
	

	
	@Execute
	public void execute(@Named(IServiceConstants.ACTIVE_SHELL) Shell shell) {
		
		Settings prop = new ProjectProperties.Settings();
		prop.setXMLDataSource();
				
		eventBroker.post("viewcommunication/asyncEvent", Settings.XML);
		
		XMLBackgroundService.INSTANCE.addBackgroundTask(sync, shell, eventBroker);
		
	}



	


	
}
