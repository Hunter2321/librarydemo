package ui.parts;


import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import models.Book;
import services.DataSourceServices;
import utility.UiTableUtilities;

public class DeleteBook {

	public TableViewer tableViewer;
	Integer selectedBookId = null;


	@Inject
	IEventBroker eventBroker;
	
	@PostConstruct
	public void createComposite(Composite parent) {
		tableViewer = new TableViewer(parent,
				SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);

		UiTableUtilities.INSTANCE.createComponents(parent, tableViewer);
		DataSourceServices.INSTANCE.setSourceOfData( tableViewer);

		DataSourceServices.INSTANCE.subscribeHandler(eventBroker, parent, tableViewer);

		final Button addBookButton = new Button(parent, SWT.PUSH);
		addBookButton.setText("Delete Book");

		tableViewer.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {

				if (!event.getSelection().isEmpty()) {
					IStructuredSelection selection = (IStructuredSelection) event.getSelection();
					Book firstElement = (Book) selection.getFirstElement();

					selectedBookId = firstElement.getId();
				}
			}
		});

		addBookButton.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent event) {
				deleteBookById(selectedBookId);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				deleteBookById(selectedBookId);

			}

		});

	}

	private void deleteBookById(Integer id) {


		if (id != null) {
			
			DataSourceServices.INSTANCE.getDataSourceProvider().deleteBook(id);
		}

		tableViewer.setSelection(null);
		selectedBookId = null;
		tableViewer.refresh();

	}

	@Focus
	public void setFocus() {

		DataSourceServices.INSTANCE.setSourceOfData( tableViewer);

		tableViewer.getTable().setFocus();
		tableViewer.refresh();
	}


	



}
