package ui.parts;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.swt.widgets.Group;
import org.eclipse.e4.ui.model.application.MApplication;

import org.eclipse.e4.ui.workbench.modeling.EPartService;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import models.Book;
import services.DataSourceServices;

public class AddBook {

	boolean isBookBorrowed;
	Text titleTextField;
	Text authorTextField;
	Text releaseDateTextField;
	Button bookIsBorrowedButton;
	Button bookIsntBorrowedButton;
	@Inject
	EPartService partService;

	@Inject
	MApplication app;

	@PostConstruct
	public void createComposite(Composite parent) {
		parent.setLayout(new GridLayout(1, false));

		GridData gd_text = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd_text.horizontalIndent = 8;

		// title text label
		Label enterTitleLabel = new Label(parent, SWT.NONE);
		enterTitleLabel.setText("Please enter title:");

		// title text field
		titleTextField = new Text(parent, SWT.BORDER);
		titleTextField.setLayoutData(gd_text);

		// author text label
		Label authorLabel = new Label(parent, SWT.NONE);
		authorLabel.setText("Please enter author:");

		authorTextField = new Text(parent, SWT.BORDER);
		authorTextField.setLayoutData(gd_text);

		// release text label
		Label releaseLabel = new Label(parent, SWT.NONE);
		releaseLabel.setText("Please enter release Date:");

		releaseDateTextField = new Text(parent, SWT.BORDER);
		releaseDateTextField.setLayoutData(gd_text);

		// is borrowed text label
		Label isBorrowedStatusLabel = new Label(parent, SWT.NONE);
		isBorrowedStatusLabel.setText("Is book borrowed?");

		Group radioButtonGroup = new Group(parent, SWT.NONE);
		radioButtonGroup.setLayout(new RowLayout(SWT.HORIZONTAL));

		bookIsBorrowedButton = new Button(radioButtonGroup, SWT.RADIO);
		bookIsBorrowedButton.setText("Yes");

		bookIsntBorrowedButton = new Button(radioButtonGroup, SWT.RADIO);
		bookIsntBorrowedButton.setText("No");

		bookIsBorrowedButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				Button source = (Button) e.getSource();

				if (source.getSelection()) {
					isBookBorrowed = true;
				}
			}
		});

		bookIsntBorrowedButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				Button source = (Button) e.getSource();

				if (source.getSelection()) {
					isBookBorrowed = false;
				}
			}
		});

		final Button addBookButton = new Button(parent, SWT.PUSH);
		addBookButton.setText("Add new book to library");

		addBookButton.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent event) {
				createBook(titleTextField.getText(), authorTextField.getText(), releaseDateTextField.getText(),
						isBookBorrowed);

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				createBook(titleTextField.getText(), authorTextField.getText(), releaseDateTextField.getText(),
						isBookBorrowed);

			}

		});

	}

	private void createBook(String title, String author, String releaseDate, boolean isBorrowed) {
		Book newBook = new Book();

		newBook.setTitle(title);
		newBook.setAuthor(author);
		newBook.setReleaseDate(releaseDate);
		newBook.setIsBorrowed(isBorrowed);

		DataSourceServices.INSTANCE.getDataSourceProvider().addBook(newBook);

		titleTextField.setText("");
		authorTextField.setText("");
		releaseDateTextField.setText("");
		bookIsBorrowedButton.setSelection(false);
		bookIsntBorrowedButton.setSelection(false);

	}

}
