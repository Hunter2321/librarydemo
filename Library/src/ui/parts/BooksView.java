package ui.parts;


import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.di.Persist;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;

import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;

import org.eclipse.swt.widgets.Composite;

import services.DataSourceServices;
import utility.UiTableUtilities;

public class BooksView {

	public TableViewer tableViewer;

	@Inject
	private IEventBroker eventBroker;

	@Inject
	private MPart part;

	@PostConstruct
	public void createComposite(Composite parent) {
		tableViewer = new TableViewer(parent,
				SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);

		UiTableUtilities.INSTANCE.createComponents(parent, tableViewer);
		DataSourceServices.INSTANCE.setSourceOfData( tableViewer);
		DataSourceServices.INSTANCE.subscribeHandler(eventBroker, parent, tableViewer);
		
	}

	

	@Focus
	public void setFocus() {

		DataSourceServices.INSTANCE.setSourceOfData( tableViewer);
		tableViewer.getTable().setFocus();

	}

	@Persist
	public void save() {
		part.setDirty(false);
	}

	
}