package data.xml;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartDocument;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import models.Book;
import properties.ProjectProperties.Settings;

public class XMLNodeEditor {

	private String xmlFile = Settings.EDITOR_FILE_PATH;

	List<Book> newList = null;

	public void createNode(String title, String author, String releaseDate, boolean isBorrowed) {
		newList = getCurrentBooksList();
		Book newBook = new Book(getNextId(), title, author, releaseDate, isBorrowed);

		newList.add(newBook);
		rewriteXmlSource();
	}
	
	public void removeBook(Book book) {
		
		newList = getCurrentBooksList();
			
		Book elementToRemove = null;
			
		for(Book bookItem : newList) {
			if(book.getId() == bookItem.getId()) {

				elementToRemove = bookItem;
			}
		}
		
		if(elementToRemove !=null) {
			newList.remove(elementToRemove);
		}
		
		rewriteXmlSource();
		
	}
	
	
	public void changeBookStatus(int bookId) {
		
		newList = getCurrentBooksList();
		Book elementToChange = null;
		
		for(Book bookItem : newList) {
			if(bookId == bookItem.getId()) {

				elementToChange = bookItem;
			}
		}
		
		if(elementToChange!=null) {
			
			
			int index = newList.indexOf(elementToChange);
			
			boolean isCurrentBookBorrowed = XMLBooksProvider.INSTANCE.checkIsBookBorrowed(elementToChange);
			
			if(isCurrentBookBorrowed) {
				
				elementToChange.setIsBorrowed(false);
				
			}else {
				
				elementToChange.setIsBorrowed(true);

				
			}
			
			
			newList.set(index, elementToChange);
			rewriteXmlSource();
		}
		
	}


	@SuppressWarnings("unchecked")
	private List<Book> getCurrentBooksList() {
		return (List<Book>) XMLBooksProvider.INSTANCE.getBooks();
	}
	
	

	
	private void rewriteXmlSource() {

		try {
			
	
			

		      // create an XMLOutputFactory
	        XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
	        // create XMLEventWriter
	        XMLEventWriter eventWriter = outputFactory
	                .createXMLEventWriter(new FileOutputStream(xmlFile));
	        // create an EventFactory
	        XMLEventFactory eventFactory = XMLEventFactory.newInstance();
	        XMLEvent end = eventFactory.createDTD("\n");
	        // create and write Start Tag
	        StartDocument startDocument = eventFactory.createStartDocument();
	        eventWriter.add(startDocument);

	        // create config open tag
	        StartElement xmlBooksStartElement = eventFactory.createStartElement("",
	                "", "xmlBooksList");
	        eventWriter.add(xmlBooksStartElement);
	        eventWriter.add(end);
	        // Write the different nodes

	        for(Book book : newList) {
	        	
	        	StartElement bookStartElement = eventFactory.createStartElement("",
		                "", "book");

	        	eventWriter.add(bookStartElement);
	        	
				eventWriter.add(eventFactory.createAttribute("id", String.valueOf(book.getId())));

		        eventWriter.add(end);

	        	createNode(eventWriter,"title", book.getTitle());
	        	createNode(eventWriter,"author", book.getAuthor());

	        	createNode(eventWriter,"date", book.getReleaseDate());
	        	createNode(eventWriter,"isBorrowed", book.getIsBorrowed().toString());

	        	eventWriter.add(eventFactory.createEndElement("", "", "book"));
		        eventWriter.add(end);

	        }

	        eventWriter.add(eventFactory.createEndElement("", "", "xmlBooksList"));
	        eventWriter.add(end);
	        eventWriter.add(eventFactory.createEndDocument());
	        eventWriter.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void createNode(XMLEventWriter eventWriter, String name, String value) throws XMLStreamException {

		XMLEventFactory eventFactory = XMLEventFactory.newInstance();
		XMLEvent end = eventFactory.createDTD("\n");
		XMLEvent tab = eventFactory.createDTD("\t");
		// create Start node
		StartElement sElement = eventFactory.createStartElement("", "", name);
		eventWriter.add(tab);
		eventWriter.add(sElement);
		// create Content
		Characters characters = eventFactory.createCharacters(value);
		eventWriter.add(characters);
		// create End node
		EndElement eElement = eventFactory.createEndElement("", "", name);
		eventWriter.add(eElement);
		eventWriter.add(end);

	}
	private int getNextId() {

		if (getCurrentBooksList().size() != 0) {
			Book lastBook = getCurrentBooksList().get(getCurrentBooksList().size() - 1);
			int lastId = lastBook.getId();

			return lastId + 1;

		} else {
			return 1;
		}

	}

}
