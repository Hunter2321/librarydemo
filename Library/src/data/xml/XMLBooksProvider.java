package data.xml;


import java.util.List;
import models.Book;


import api.BooksProviderInterface;

public enum XMLBooksProvider implements BooksProviderInterface {
	INSTANCE;

	XMLReader newReader = new XMLReader();
	XMLNodeEditor xmlNodeEditor = new XMLNodeEditor();

	@Override
	public List<? extends Object> getBooks() {

		
		return newReader.getXMLBooksList();
	}

	@Override
	public void addBook(Object book) {

		Book bookToAdd = (Book) book;
		xmlNodeEditor.createNode(bookToAdd.getTitle(), bookToAdd.getAuthor(), bookToAdd.getReleaseDate(),
				bookToAdd.getIsBorrowed());

	}

	@Override
	public void deleteBook(int id) {

		xmlNodeEditor.removeBook((Book) getBookById(id));

	}

	@Override
	public void changeBorrowedStatusOfBook(int bookId) {

		xmlNodeEditor.changeBookStatus(bookId);

	}

	@Override
	public Object getBookById(int id) {

		Book bookById = null;
		for (Book book1 : newReader.getXMLBooksList()) {
			if (book1.getId() == id) {
				bookById = book1;
			}
		}

		return bookById;

	}

	@SuppressWarnings("null")
	@Override
	public boolean checkIsBookBorrowed(Object book) {

		int bookId = ((Book) book).getId();
		List<Book> list = newReader.getXMLBooksList();

		Book bookToCheck = null;

		for (Book item : list) {
			if (item.getId() == bookId) {
				bookToCheck = item;
			}
		}


		if (bookToCheck != null) {
			return bookToCheck.getIsBorrowed();
		} else {
			return (Boolean) null;
		}

	}

}
