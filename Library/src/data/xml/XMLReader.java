package data.xml;


import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import models.Book;
import properties.ProjectProperties.Settings;

public class XMLReader {

	static final String BOOK = "book";
	static final String ID = "id";
	static final String TITLE = "title";
	static final String AUTHOR = "author";
	static final String DATE = "date";
	static final String IS_BORROWED = "isBorrowed";


	private String xmlFile = Settings.READER_FILE_PATH;

	private InputStream createin(){

		return getClass().getResourceAsStream(xmlFile);

	}



	@SuppressWarnings({ "unchecked", "null" })
	public List<Book> getXMLBooksList() {
		ArrayList<Book> books = new ArrayList<Book>();

		try {

			// First, create a new XMLInputFactory
			XMLInputFactory inputFactory = XMLInputFactory.newInstance();
			// Setup a new eventReader
//			in = getClass().getResourceAsStream(file);

			XMLEventReader eventReader;
			
			
			eventReader = inputFactory.createXMLEventReader(createin());
			// read the XML document
			Book book = null;

			// je�li ma event Reader ma nast�pny event
			while (eventReader.hasNext()) {
				XMLEvent event = eventReader.nextEvent();

				if (event.isStartElement()) {
					StartElement startElement = event.asStartElement();
					// If we have an item element, we create a new item
					if (startElement.getName().getLocalPart().equals(BOOK)) {
						book = new Book();
						// We read the attributes from this tag and add the date
						// attribute to our object
						Iterator<Attribute> attributes = startElement.getAttributes();
						while (attributes.hasNext()) {
							Attribute attribute = attributes.next();
							if (attribute.getName().toString().equals(ID)) {

								book.setId(Integer.parseInt(attribute.getValue()));
							}
						}
					}

					if (event.isStartElement()) {
						if (event.asStartElement().getName().getLocalPart().equals(TITLE)) {
							event = eventReader.nextEvent();
							book.setTitle(event.asCharacters().getData());
							continue;
						}
					}
					if (event.asStartElement().getName().getLocalPart().equals(AUTHOR)) {
						event = eventReader.nextEvent();
						book.setAuthor(event.asCharacters().getData());
						continue;
					}

					if (event.asStartElement().getName().getLocalPart().equals(DATE)) {
						event = eventReader.nextEvent();
						book.setReleaseDate(event.asCharacters().getData());
						continue;
					}

					if (event.asStartElement().getName().getLocalPart().equals(IS_BORROWED)) {
						event = eventReader.nextEvent();

						String a = event.asCharacters().getData().toLowerCase();

						if (String.valueOf(a).equals("true")) {
							book.setIsBorrowed(true);
						} else {
							book.setIsBorrowed(false);
						}

						continue;
					}
				}
				// If we reach the end of an item element, we add it to the list
				if (event.isEndElement()) {
					EndElement endElement = event.asEndElement();
					if (endElement.getName().getLocalPart().equals(BOOK)) {
						books.add(book);
					}
				}

			}

		}

		catch (XMLStreamException e) {
			e.printStackTrace();
		}

		return books;

	}

}
