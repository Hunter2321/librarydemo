package models;



import java.util.Iterator;
import java.util.List;



import org.eclipse.jface.viewers.IStructuredSelection;


public class Book implements IStructuredSelection {

	int id;
	String title;
	String author;
	String releaseDate;
	boolean isBorrowed;
	

	public Book() {
		
	}
	public Book(int id, String title, String author, String releaseDate, boolean isBorrowed){
		this.id = id;
		this.title = title;
		this.author = author;
		this.releaseDate = releaseDate;
		this.isBorrowed = isBorrowed;
		
	}
	
	

	
	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getAuthor() {
		return author;
	}


	public void setAuthor(String author) {
		this.author = author;
	}


	public String getReleaseDate() {
		return releaseDate;
	}


	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}


	public Boolean getIsBorrowed() {
		return isBorrowed;
	}


	public void setIsBorrowed(Boolean isBorrowed) {
		this.isBorrowed = isBorrowed;
	}
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String toString () {
		
		String isBorrowedString;
		
		if(isBorrowed) {
			isBorrowedString = "Yes";
		}else {
			isBorrowedString = "No";
		}		
		
		return "Title: " + title + " Author: " + author + " Release Date: " + releaseDate + " Is book borrowed? " + isBorrowedString;
	}


	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}



	@Override
	public Object getFirstElement() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List toList() {
		// TODO Auto-generated method stub
		return null;
	}
}
