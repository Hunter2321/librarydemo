package services;



import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.widgets.Composite;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;

import api.BooksProviderInterface;
import data.local.LocalBooksProvider;
import data.xml.XMLBooksProvider;
import properties.ProjectProperties.Settings;

public enum DataSourceServices {
	INSTANCE;

	public void setSourceOfData(TableViewer tableViewer) {

		tableViewer.setInput(getDataSourceProvider().getBooks());
		tableViewer.refresh();
	}

	public BooksProviderInterface getDataSourceProvider() {
		String dataSource = String.valueOf(Settings.DATA_SOURCE);

		if (dataSource == String.valueOf(Settings.XML)) {

			return XMLBooksProvider.INSTANCE;

		} else if (dataSource == String.valueOf(Settings.LOCAL)) {

			return LocalBooksProvider.INSTANCE;
		} else {
			return null;
		}
	}

	private EventHandler createHandler(Composite parent, TableViewer tableViewer) {
		EventHandler handler = new EventHandler() {
			public void handleEvent(final Event event) {

				parent.getDisplay().asyncExec(new Runnable() {
					public void run() {

						setSourceOfData(tableViewer);
					}
				});
			}
		};
		return handler;
	}


	public void subscribeHandler(IEventBroker eventBroker, Composite parent, TableViewer tableViewer) {
		eventBroker.subscribe("viewcommunication/*", createHandler(parent, tableViewer));

	}


}
