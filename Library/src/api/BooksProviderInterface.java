package api;

import java.util.List;

public interface BooksProviderInterface {

	List<? extends Object> getBooks();
	void addBook(Object book);
	void deleteBook(int id);
	Object getBookById(int id);
	boolean checkIsBookBorrowed(Object book);
	void changeBorrowedStatusOfBook(int bookId);
}
