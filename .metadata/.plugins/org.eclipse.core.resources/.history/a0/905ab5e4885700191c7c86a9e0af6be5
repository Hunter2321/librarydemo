package services;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.ICoreRunnable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.UISynchronize;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;

import data.xml.XMLBooksProvider;
import models.Book;
import properties.ProjectProperties.Settings;

public enum XMLBackgroundService {
	INSTANCE;

	private UISynchronize sync = null;
	private Job job;
	private Shell shell = null;
	private IEventBroker eventBroker;

	private boolean shouldRunBackgroundTask = true;

	private List<Book> firstList;
	private List<Book> secondList;

	private boolean isXmlDataChanged = false;
	private String changeInfo = "";

	private final String BOOK_ADDED = "Book has been added!";
	private final String BOOK_REMOVED = "Book has been removed!";
	
	

	private ICoreRunnable iCoreRunnable = new ICoreRunnable() {
		@Override
		public void run(IProgressMonitor monitor) throws CoreException {
			if (shouldRunBackgroundTask) {

				setXmlDataChangeStatus();

				if (isXmlDataChanged) {
					sync.syncExec(() -> {
						eventBroker.post("viewcommunication/asyncEvent", Settings.XML);

						MessageDialog.openInformation(shell, "Info", changeInfo);

					});
				}

				job = Job.create("Check xml source", iCoreRunnable);
				job.schedule(5000);		
			}
		}

	};

	@SuppressWarnings("unchecked")
	public void addBackgroundTask(UISynchronize sync, Shell shell, IEventBroker eventBroker) {

		firstList = (List<Book>) XMLBooksProvider.INSTANCE.getBooks();
		shouldRunBackgroundTask = true;
		this.sync = sync;
		this.shell = shell;
		this.eventBroker = eventBroker;
		
		job = Job.create("Check xml source", iCoreRunnable);
		job.schedule();

	}

	public void stopBackgroundTask() {

		if (job != null) {
			shouldRunBackgroundTask = false;
			job.cancel();
		}

	}
	
	
	@SuppressWarnings("unchecked")
	private void setXmlDataChangeStatus() {
		secondList = (List<Book>) XMLBooksProvider.INSTANCE.getBooks();
		isXmlDataChanged = true;
		if (firstList.size() < secondList.size()) {
			changeInfo = BOOK_ADDED;

		} else if (firstList.size() > secondList.size()) {
			changeInfo = BOOK_REMOVED;

		} else if(isAnyBookChanged()) {
			
			
			for (int i = 0; i < firstList.size(); i++) {

				createBookStatusChangeInfo(i);
			}
		
		} else {
			isXmlDataChanged = false;
		}

		firstList = secondList;
	}
	
	
	private void createBookStatusChangeInfo(int index) {
		
		
		boolean firstBookStatus = (boolean) firstList.get(index).getIsBorrowed();
		Book secondBook = (Book) secondList.get(index);

		if (firstBookStatus != secondBook.getIsBorrowed()) {
			
			String statusBookName = "";

			if(secondBook.getIsBorrowed()) {
				statusBookName = "BORROWED";
			} else {
				statusBookName = "FREE";
			}
			changeInfo = "\"" + secondBook.getTitle() + "\"" + " status changed into: " + statusBookName ;
		}
		
		
	}
	
	

	private boolean isAnyBookChanged() {
		
		
		int firstListSize = firstList.size();
		int secondListSize = secondList.size();

		boolean isStatusBookChanged = false;
		
		if (firstListSize == secondListSize) {

			for (int i = 0; i < firstListSize; i++) {

				boolean firstBookStatus = (boolean) firstList.get(i).getIsBorrowed();
				boolean secondBook = (boolean) secondList.get(i).getIsBorrowed();
				if (firstBookStatus != secondBook) {
					isStatusBookChanged = true;
					break;
				}
			}
		}
		
		return isStatusBookChanged;
		
	}



}
